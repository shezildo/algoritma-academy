- RStudio is an IDE (environment to write code)
- Use `read.csv()` to read a dataset into your environment
    + R works with csv, xlsx (excel), API (JSON or XML), webscraping, SQL
    + Alternative: `fread()` to read say 10 million rows of data
- Check your first 6 rows of data using `head()`
    + Alternative: `tail()`
- Inspect the **structure** of your data using `str()`
- Get a **summary** of your data using `summary()`
    + Alternative: `table(startups$breakeven)`
    + Alternative: `prop.table()`
- Other functions to call on dataset: `names()`
- Data Transformation
    + `as.Date()` to convert to a date
    + `as.numeric()` to convert to a numeric
    + `as.factor()` to convert to a factor (category)
    + `as.character()` to convert to a character

- Monte-Carlo Simulation
    + `set.seed()`
    + `rbinom()`, `sample(c("high", "medium", "low"), 5, replace=T)`, `rnorm()`

- Combining data
    + `dim()` to get a compact idea of number of rows and columns
    + `ncol` and `nrow`
    + Proceed with `cbind()` which is to bind multiple vectors / lists side-by-side
    + Proceed with `rbind()` to bind by row

- Arrange data
    + Use `sort()`, use `order()` use `decreasing=T` if necessary
    + Find the index that contains the maximum value using `which.max()`

- Statistical
    + `mean()`, `median()`, `quantile(salary, 0.8)`, `min()`, `max()`
    + `diff(quantile(salary, c(0.25, 0.75)))` would be the same as `IQR(salary)`
    + `hist(salary)`
    + Export this data we cleaned and transformed using `write.csv()`







