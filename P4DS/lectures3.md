# Notes and General Programming Tips
- Use `library()` in RMD, do not include `install.packages()`
- Loading of library **do matters**, if you need to, then
    be explicit by specifying your function call like `skimr::skim()` 
- `getRversion()` to know what version of R is running
- Use `ls()` to know what you've created in your environment
- Use `rm(list=ls())` to remove everything in your environment
- When you write a script, use `source()` to read that script and export
the functions into your environment
- When you update that script, you can specify "source on save"
- Sign up for Rpubs (free account) so you can immediately publish
    any of your analysis or data work online with your own url