# Common Workflow to Data Analysis
1. Reading in data (`read.csv(tvreviews)`)
2. Use `str()`, `summary()`, `prop.table()`, 
    `head(tvreviews)`
    to get a sense of your data
3. Use `anyNA` to look for any missing values
4. Think about types
    - `as.Factor()`
    - `as.numeric()`
    - `as.integer()`
    - `as.character()`
5. If missing values, deal with missing values
    - Complete Cases
    - Imputation

## Exploratory Data Analysis
1. Find patterns in our data
    - Find correlation
    - Simpler statistics
        + `mean()`, `median()`, `min()`, `max()`
2. Sometimes its easier to find patterns using visuals
3. If **one-dimensional**
    - plot(tvreviews$rating)
    - hist(tvreviews$rating)
    - density(tvreviews$rating)
    - boxplot(tvreviews$rating)
        + boxplot is not commonly used in isolation.
        + Use it to compare data
4. If **two-dimensional**
    - `plot(x=tvreviews$gender, y=tvreviews$rating)`
    - Do a two-dimensional table using `table()` 

## Advanced Statistical Analysis
1. Thinking about `aggregate()` 
2. Use a custom function in addition to `sum`, `mean` etc
3. If you need to have a sum, then just use `xtabs()`
4. Tease out correlation between variables using `cor()` 
   and the `pairs()`


