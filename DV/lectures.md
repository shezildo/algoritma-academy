# Day 1 summary

Run `plot()` by default it picks the right `type` based
on the input data. 
    - If x and y are both numeric, pick a `p`: point
    - If x is categorical and y numeric, pick a boxplot

Doesn't need `x` and `y`. You can use `plot` with only
one variable

Use extra parameters (most of these are optional):
    `main="Crime rate in Jakarta, Q1 2017"` for main title
    `col="dodgerblue4"` for coloring your plot
    `pch=19` for a different point character
    `cex=0.8` shrinks / expand the point size

After you created a plot using `plot`, add elements to plot:
    `legend()` to add a legend
    `abline()` specifying line width (`lwd`) or type (`lty`)

Avoid using a pie chart, but if you have to: `pie()`

## ggplot
1. Create a `ggplot` object (canvas)
    data: your dataframe
    aes: your `x` and `y`

2. Add geometry to your `ggplot` object
    +`geom_line()`
    +`geom_point()`
    +`geom_boxplot()`
    +`geom_col()`
    +`geom_label()`

3. Add labels to your `ggplot` object
    +`labs(title, subtitle, caption, x, y)`

4. Control position of your legend
    +`theme(legend.position = "bottom")`

5. Also possible:
    +`geom_point(aes(size=comment), col="blue")`

Start to finish:
    ggplot(data=crm, aes(x=segment, y=risk))+
        geom_point(col="red")+
        geom_line(aes(col=debtmaturity))+
        labs(x="Segment", y="Customer Risk", subtitle="Risk went up 41% in Q3")+ theme()






