# Data Visualization summary

Run `plot()` by default it picks the right `type` based
on the input data. 
    - If x and y are both numeric, pick a `p`: point
    - If x is categorical and y numeric, pick a boxplot

Doesn't need `x` and `y`. You can use `plot` with only
one variable

Use extra parameters (most of these are optional):
    `main="Crime rate in Jakarta, Q1 2017"` for main title
    `col="dodgerblue4"` for coloring your plot
    `pch=19` for a different point character
    `cex=0.8` shrinks / expand the point size

After you created a plot using `plot`, add elements to plot:
    `legend()` to add a legend
    `abline()` specifying line width (`lwd`) or type (`lty`)

Avoid using a pie chart, but if you have to: `pie()`

## Reshaping
Data structures usually in "wide" format
Sometimes it's convenient to have it in "long" format
Use `library(reshape2)` and then `melt(sales)`

## ggplot
1. Create a `ggplot` object (canvas)
    data: your dataframe
    aes: your `x` and `y`

2. Add geometry to your `ggplot` object
    +`geom_line()`
    +`geom_point()`
    +`geom_boxplot()`
    +`geom_violin()`
    +`geom_col()`
    +`geom_label()`
    +`geom_text()`
    +`geom_hline()`
    +`geom_vline()`
    +`geom_rug()`


3. Add labels to your `ggplot` object
    +`labs(title, subtitle, caption, x, y)`

4. Control position of your legend
    +`theme(legend.position = "bottom", strip.text=element_text(size=7))`

5. Also possible:
    +`geom_point(aes(size=comment), col="blue")`

    When you create column plots with a long data frame:
    +`geom_col(aes(col=segment), position="dodge")`
        By default, position is `stack`

    When you use `geom_label` or `geom_text`:
        `hjust="inward"` can help prevent text from overflowing, but justifying them inwards
        `nudge_x` and `nudge_y` to nudge your text or labels slightly sideward / upward

6. Faceting
    We can use `facet_wrap()` to create facet plots that help us better visualize relationships within groups and between groups
    - Use `ncol` or `nrow` to specify the maximum row / columns

Start to finish:
    ggplot(data=crm, aes(x=segment, y=risk))+
        geom_point(col="red")+
        geom_line(aes(col=debtmaturity))+
        labs(x="Segment", y="Customer Risk", subtitle="Risk went up 41% in Q3")+ theme()







