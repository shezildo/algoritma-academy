# Descriptive Statistics
## Measures of Central Tendency
1. Mean
    - Use `mean()` to find that
    - Sensitive to extreme values
    - Trimmed mean computes the mean after eliminating the x% most
    extreme values `mean(customerrisk, trim=0.05)`
2. Median
    - Use `median()`
    - Not sensitive, but provide us a value
    that cut our distribution in equal halves
3. Mode
    - Use `table()` and then find the highest
    - Compute mode on both numerical and categorical
    variables
    - It is just the "most frequent" value / item

## Measures of Spread
1. Variance
    - Call `var()`
    - sum(differenced^2)/(n-1)
    - Sum of squared difference (averaged)
2. Standard Deviation
    - Call `sd()`
    - Be the same if `sqrt(var(customerrisk))`
    - Put this on the same scale and hence can be interpreted:
        + Our customer on average pays their credit card loan
        + in 5.6 days since due, with a standard deviation of 1.1 day
3. IQR (Interquartile Range)
    - Call `IQR()`
    - This is the **middle 50%** of your distribution
    - Be the same if:
        + `quantile(customerrisk, 0.75)` - `quantile(customerrisk, 0.25)`
4. Range
    - Call `range()`
    - Same if you just observed from the `min()` and `max()`

When you have a normal distribution with mean 0 and sd 1, we call it a standard normal distribution







